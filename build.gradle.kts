/**
 * application is needed to use `application {}`
 */
plugins {
    kotlin("jvm") version "1.3.61"
    java
    application
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

/**
 * says gradle to use `src/main/kotlin/main.kt
 */
application {
    mainClassName = "MainKt"
}

/**
 * loads and compiles `Commons CSV`
 */
dependencies {
    implementation(kotlin("stdlib-jdk8"))
    compile("org.apache.commons", "commons-csv","1.5")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}