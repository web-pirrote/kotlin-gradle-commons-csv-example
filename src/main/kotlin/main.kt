import org.apache.commons.csv.*
import java.nio.file.*

/**
 * Main entry point
 *
 */
fun main() {
    /** create a path instance */
    val filePath: Path = Paths.get("output/yeah.csv")

    /** if path doesn't exist it creates the directories from the path */
    if (! Files.exists(filePath)) {
        Files.createDirectories(filePath.parent)
    }

    /** writes List<Map<String, Any>> to file */
    val data = getData()
    write(data, filePath)

    /** reads file */
    val csvParser = read(filePath)
    toConsole(csvParser)
}

/**
 * Just to create the sample data
 *
 * @return List of maps
 */
fun getData(): List<Map<String, Any>> {
    return listOf(
        mapOf(
            "id" to 1,
            "name" to "James",
            "location" to "London",
            "company" to "Bond",
            "salary" to 16_000
        ),
        mapOf(
            "id" to 2,
            "name" to "Julia",
            "location" to "Berlin",
            "company" to "ToysUs",
            "salary" to 55_000
        ),
        mapOf(
            "id" to 3,
            "name" to "Ben",
            "location" to "Paris",
            "company" to "Scooter",
            "salary" to 34_000
        ),
        mapOf(
            "id" to 8,
            "name" to "Mike",
            "location" to "Wien",
            "company" to "DataCollect",
            "salary" to 90_000
        ),
        mapOf(
            "id" to 17,
            "name" to "Hulk",
            "location" to "Moskau",
            "company" to "DulcesDulces",
            "salary" to 136_000
        )
    )
}

/**
 * Write a list to a CSV file
 *
 * @param data A list of maps where the key represents the headline and the value the value
 * @param filePath The path to the CSV file relative from project root
 */
fun write(
    data: List<Map<String, Any>>,
    filePath: Path
)
{
    val writer = Files.newBufferedWriter(filePath)
    val csvPrinter = CSVPrinter(
        writer,
        CSVFormat.RFC4180.withHeader("id", "name", "location", "company", "salary")
    )

    /** Iterates through the list and writes the data to the CSV file */
    for (map in data) {
        csvPrinter.printRecord(map["id"], map["name"], map["location"], map["company"], map["salary"])
    }

    csvPrinter.flush()
    csvPrinter.close()
}

/**
 * Reads and parses a csv file
 *
 * @param filePath The path to the CSV file relative from project root
 * @return
 */
fun read(
    filePath: Path
):  CSVParser
{
    val reader = Files.newBufferedReader(filePath)

    return CSVParser(
        reader,
        CSVFormat
            .DEFAULT
            .withFirstRecordAsHeader()
            .withIgnoreHeaderCase()
            .withTrim()
    )
}

/**
 * prints CSVParser to console
 *
 * @param csvParser created with Commons CSV
 */
fun toConsole(
    csvParser: CSVParser
)
{
    val header = csvParser.headerMap.keys
    var consoleText = ""

    /** Iterates through the parsed CSV and prints it to the console */
    for (csvRecord in csvParser) {
        var datasetString = ""
        val recordNumber = csvRecord.recordNumber

        /** record headline */
        datasetString += "| $recordNumber Row |\n"

        /** prints out the data dynamically */
        for (head in header) {
            datasetString += "${head.toUpperCase()}: ${csvRecord[head]}\n"
        }

        /** Separator */
        consoleText += "\n$datasetString\n------------------------------------*\n"
    }

    p(consoleText)
}

/**
 * Abbreviation for println
 *
 * @param t type
 * @param v variable to be printed
 */
fun <t>p(v: t) = println(v)