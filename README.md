# Kotlin + Commons CSV - Example

Hi welcome to my repo. \
This repo is for kotlin beginners and persons who aren't familiar to the java world.
This repo uses gradle with kotlin DSL (instead of Groovy) for building and running.

Gradle helps you to retrieve dependencies like libraries, compiling everything.. and finally running the code.
For tutorials visit the official page or use a search engine.

Neither java nor kotlin have CSV parsing... in their standard libraries. 
But I wanted to know how is it possible to read and write CSV with kotlin. 
I thought there must be an easy way but there wasn't. 
So I decided to make my success public and maybe someone has the same problem as me and it helps him. 
So I come originally from PHP, so most time you just make a php file include, or you use composer und then you are done. 
But with kotlin you have to include something already compiled and then the JVM...

I believe this is the best way to show how to include a java library, that even is intended to be used with Maven, in kotlin.

FYI: You don't have to, but I used intellij for coding.

### Build
open the project in a terminal and navigate to the project root.\
here you have to execute `gradlew` the gradle wrapper.\
In linux you just do:
```shell script
./gradlew build
```

### Run
The same as build but with run
```shell script
./gradlew run
```

#### See more commands 
```shell script
./gradlew tasks
```